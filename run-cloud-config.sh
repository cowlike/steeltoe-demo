#!/bin/bash

docker run --rm -d --name scc -p 8888:8888 \
    -e SPRING_PROFILES_ACTIVE=native \
    -e SPRING_CLOUD_CONFIG_SERVER_NATIVE_SEARCHLOCATIONS="file:///config, file:///appconfig" \
    -v $(pwd)/appSearchLocation:/appconfig \
    -v $(pwd)/serverconfig:/config \
    hyness/spring-cloud-config-server
