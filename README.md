# Steeltoe Configuration Demo

## Overview

Demo of [Steeltoe config provider](https://steeltoe.io/docs/steeltoe-configuration/#2-0-config-server-provider/). This adds a .NET Core IConfigurationProvider that reads from a Spring Cloud Config server.

Detailed configuration options [for Steeltoe provider](https://steeltoe.io/docs/steeltoe-configuration/)

## Run stdemo services in a local dev swarm (spring cloud config and vault)

```bash
# startup
docker swarm init
docker stack deploy -c docker-compose.yml stdemo

# optionally look at some things
docker service ls
docker service ps stdemo_scc
docker inspect stdemo_vault
docker container ls -q
docker service logs -f stdemo_vault

# set some properties in Vault
docker exec -it $(docker ps |grep stdemo_vault |cut -d ' ' -f1)  sh /scripts/addVaultProps.sh

# and remove them from Vault
docker exec -it $(docker ps |grep stdemo_vault |cut -d ' ' -f1)  sh /scripts/rmVaultProps.sh

# after updating some service properties, update the service
docker service update --force stdemo_scc

# shutdown
docker stack rm stdemo
docker swarm leave --force
```

## Run the app and monitor some parameters

```bash
#run this app
dotnet run foo global1 p1 p2

#run using QA configuration
DEPLOY=qa dotnet run foo global1 p1 p2
```

The app will run forever, pausing 5 seconds between prints. While the app is running, change the `p1` or `p2` property in application property file and observe the change. The command line arguments passed to the program are the config variable names to display. In the example above, the properties being displayed are `global1`, `p1` and `p2`.

The properties that are loaded correspond to the application name in the `appsettings.json` file. The `data/application.yml`. In the included appsettings, this is set to `myapp_2`. This means that the properties in `appSearchLocation/myapp_2.properties` are the ones that are loaded into Spring Cloud Config (SCC). Since SCC is running in a Docker container, we use volume mounts to mount these local configuration folders inside the container (see `run-cloud-config.sh`).

`myapp_2` has 3 different property files, a default one for DEV (`myapp_2.properties`), one for QA (`myapp_2-qa.properties`) and one for PROD (`myapp_2-prod.properties`). If you don't set the environment variable `DEPLOY` or if there is no corresponding property file, the default property file is used.

Note that the order of the paths in `SPRING_CLOUD_CONFIG_SERVER_NATIVE_SEARCHLOCATIONS` is important. In order for the app-specific configuration to override any values set in the servers's configuration, the app property folder must come last.
