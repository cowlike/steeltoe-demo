﻿open System.IO
open System.Threading
open Steeltoe.Extensions.Configuration.ConfigServer
open Microsoft.Extensions.Configuration
open TMon.Utils

let rec show (cfg: IConfigurationRoot) args =
  Array.iter (fun s -> printfn "%s=%s" s (cfg.Item s)) args
  printfn "---"
  Thread.Sleep 5000
  try
    cfg.Reload()
  with e ->
    printfn "Exception reading config: %s" e.Message
  show cfg args

[<EntryPoint>]
let main argv =
  let retval =
    try
      let cfg =
        ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json", false, true)
          .AddConfigServer(cfgDef "DEV" "DEPLOY")
          .Build()

      show cfg argv
      0
    with e ->
      printfn "Error starting app: %s" e.Message
      1
  retval
